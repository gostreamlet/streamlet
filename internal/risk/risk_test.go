package risk

import (
	"gitlab.com/gostreamlet/streamlet/streamlet"
	"strings"
	"testing"
	"time"
)

func autoCompleteTasks(engine *streamlet.BPMNEngine) {
	for {
		piCmdResp, _ := (*engine).SendCommand(streamlet.GetProcessInstancesCmd{})
		piResp := piCmdResp.(streamlet.GetProcessInstancesResp)
		if piResp.Data == nil || len(piResp.Data) == 0 {
			return
		}
		taskCmdResp, _ := (*engine).SendCommand(streamlet.GetTasksCmd{})
		taskResp := taskCmdResp.(streamlet.GetTasksResp)

		for _, t := range taskResp.Data {
			var data any
			if t.Key == "DetermineApprovalTask" {
				data = ApproveTask{
					Approved: true,
					Comments: "Looks good for approval",
				}
			} else {
				data = streamlet.BaseTask{
					CompletedBy: streamlet.CompletedBy{
						CompletedById:   "123",
						CompletedByUser: "John Doe",
					},
					Comments: streamlet.Comments{Comments: "Comments"},
				}
			}
			(*engine).SendCommand(streamlet.CompleteUserTasksCmd{Tasks: []streamlet.CompleteUserTaskCmd{{
				ProcessInstanceId: t.ProcessInstanceId,
				TaskId:            t.Id,
				Data:              data,
			}}})
		}
		time.Sleep(1 * time.Millisecond)
	}
}

func TestRisk90Low(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, Risk{Score: 90})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "LowRiskTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "MediumRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "HighRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "DetermineApprovalTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.NoneEndEventType, "ApprovedEndEvent", streamlet.ElementCompleted, 1)
}

func TestRisk91Low(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, Risk{Score: 91})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "LowRiskTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "MediumRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "HighRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "DetermineApprovalTask", streamlet.ElementCompleted, 1)
}

func TestRisk75Medium(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, Risk{Score: 75})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "LowRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "MediumRiskTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "HighRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "DetermineApprovalTask", streamlet.ElementCompleted, 1)
}

func TestRisk89Medium(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, Risk{Score: 89})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "LowRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "MediumRiskTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "HighRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "DetermineApprovalTask", streamlet.ElementCompleted, 1)
}

func TestRisk74High(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewRiskProcess(id, engine, startEvent, Risk{Score: 74})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "LowRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "MediumRiskTask", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "HighRiskTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "DetermineApprovalTask", streamlet.ElementCompleted, 1)
}

func TestUnmarshalLowRiskTask(t *testing.T) {
	data := `{
		"tasks": [
			{"processInstanceId":"123", "taskId": "45", "taskKey": "LowRiskTask", "data": {"completedById": "1", "completedByUser": "joe", "comments": "test"}}	
		]
	}`
	cmd, err := UnmarshalTask(strings.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	baseTask := cmd.Tasks[0].Data.(streamlet.BaseTask)
	if baseTask.Comments.Comments != "test" {
		t.Errorf("expected baseTask.Comments.Comments == '%s', got '%s'", "test", baseTask.Comments.Comments)
	}
}

func TestUnmarshalDetermineApprovalTask(t *testing.T) {
	data := `{
		"tasks": [
			{"processInstanceId":"123", "taskId": "45", "taskKey": "DetermineApprovalTask", "data": {"approved": true, "comments": "test"}}	
		]
	}`
	cmd, err := UnmarshalTask(strings.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	appTask := cmd.Tasks[0].Data.(ApproveTask)
	if appTask.Approved != true {
		t.Errorf("expected appTask.Approved == true, got false")
	}

	if appTask.Comments != "test" {
		t.Errorf("expected appTask.Comments == '%s', got '%s'", "test", appTask.Comments)
	}
}

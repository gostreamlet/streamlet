package risk

import (
	"bytes"
	"encoding/json"
	"io"
	"strings"
	"sync"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

const (
	StartEvent_1 = "StartEvent_1"

	RiskLevelSequenceFlow = "RiskLevelSequenceFlow"

	SequenceFlowLow = "SequenceFlowLow"

	SequenceFlowMedium = "SequenceFlowMedium"

	SequenceFlowHigh = "SequenceFlowHigh"

	SequenceFlow_0l5tqx1 = "SequenceFlow_0l5tqx1"

	SequenceFlow_0w7t5n9 = "SequenceFlow_0w7t5n9"

	SequenceFlow_0lbbufh = "SequenceFlow_0lbbufh"

	SequenceFlow_0ws1ehw = "SequenceFlow_0ws1ehw"

	SequenceFlow_11dlpfx = "SequenceFlow_11dlpfx"

	SequenceFlow_1or3ltq = "SequenceFlow_1or3ltq"

	SequenceFlow_1wm77ao = "SequenceFlow_1wm77ao"

	LowRiskTask = "LowRiskTask"

	HighRiskTask = "HighRiskTask"

	MediumRiskTask = "MediumRiskTask"

	DetermineApprovalTask = "DetermineApprovalTask"

	ExclusiveGateway_11ubehw = "ExclusiveGateway_11ubehw"

	ExclusiveGateway_0dxnubq = "ExclusiveGateway_0dxnubq"

	ExclusiveGateway_0etqaxv = "ExclusiveGateway_0etqaxv"

	ApprovedEndEvent = "ApprovedEndEvent"

	EndEvent_03793jr = "EndEvent_03793jr"
)

func NewRiskProcess(id string, bpmnEngine *streamlet.BPMNEngine, startElement streamlet.ElementStateProvider, risk Risk) *streamlet.ProcessInstance {
	impl := RiskProcess{sync.RWMutex{}, &risk}
	pi := streamlet.ProcessInstance{Key: "RiskProcess", Id: id, BpmnEngine: bpmnEngine, Status: streamlet.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type RiskProcess struct {
	mu   sync.RWMutex
	risk *Risk
}

func (p *RiskProcess) ProcessInstanceData() any {
	return *p.risk
}

func UnmarshalTask(r io.Reader) (streamlet.CompleteUserTasksCmd, error) {
	tasksCmd := streamlet.CompleteUserTasksCmd{Tasks: make([]streamlet.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := streamlet.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case LowRiskTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case HighRiskTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case MediumRiskTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case DetermineApprovalTask:
			var taskData ApproveTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *streamlet.BPMNEngine, key string) T {
	states, err := engine.StateStore.GetElementStates(streamlet.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*streamlet.UserTask[T]).Data
}

func (p *RiskProcess) GetNextElement(engine *streamlet.BPMNEngine, g streamlet.IdGenerator, currentElement streamlet.ElementStateProvider) streamlet.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartEvent_1:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(RiskLevelSequenceFlow, id)

	case RiskLevelSequenceFlow:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if p.risk.Score >= 90 {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlowLow, id)
			} else if p.risk.Score >= 75 && p.risk.Score < 90 {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlowMedium, id)
			} else if p.risk.Score < 75 {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlowHigh, id)
			}
			panic("No Default Flow!")

			return nil
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_11ubehw, id, gwHandler)

	case SequenceFlowLow:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](LowRiskTask, id)

	case SequenceFlowMedium:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](MediumRiskTask, id)

	case SequenceFlowHigh:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](HighRiskTask, id)

	case SequenceFlow_0l5tqx1:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			id := g.GenerateId(streamlet.SequenceFlowType)
			return streamlet.NewSequenceFlow(SequenceFlow_0w7t5n9, id)
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_0w7t5n9:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[ApproveTask](DetermineApprovalTask, id)

	case SequenceFlow_0lbbufh:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			id := g.GenerateId(streamlet.SequenceFlowType)
			return streamlet.NewSequenceFlow(SequenceFlow_0w7t5n9, id)
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_0ws1ehw:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			id := g.GenerateId(streamlet.SequenceFlowType)
			return streamlet.NewSequenceFlow(SequenceFlow_0w7t5n9, id)
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0dxnubq, id, gwHandler)

	case SequenceFlow_11dlpfx:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if TaskData[ApproveTask](engine, DetermineApprovalTask).Approved && strings.Contains(TaskData[ApproveTask](engine, DetermineApprovalTask).Comments, "good") {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlow_1or3ltq, id)
			} else {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlow_1wm77ao, id)
			}
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0etqaxv, id, gwHandler)

	case SequenceFlow_1or3ltq:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(ApprovedEndEvent, id)

	case SequenceFlow_1wm77ao:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(EndEvent_03793jr, id)

	case LowRiskTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0l5tqx1, id)

	case HighRiskTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0ws1ehw, id)

	case MediumRiskTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0lbbufh, id)

	case DetermineApprovalTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_11dlpfx, id)

	case ExclusiveGateway_11ubehw:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0dxnubq:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0etqaxv:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ApprovedEndEvent:
		return nil

	case EndEvent_03793jr:
		return nil

	}
	return nil
}

package plan

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"gitlab.com/gostreamlet/streamlet/streamlet"
)

const (
	StartEvent_1 = "StartEvent_1"

	SequenceFlow_1rgyt8e = "SequenceFlow_1rgyt8e"

	SequenceFlow_0hbc8s4 = "SequenceFlow_0hbc8s4"

	SequenceFlow_04cepq3 = "SequenceFlow_04cepq3"

	SequenceFlow_0tnr42a = "SequenceFlow_0tnr42a"

	SequenceFlow_0zwsz2i = "SequenceFlow_0zwsz2i"

	SequenceFlow_1fdvgyu = "SequenceFlow_1fdvgyu"

	SequenceFlow_11ovoow = "SequenceFlow_11ovoow"

	SequenceFlow_0dpgu80 = "SequenceFlow_0dpgu80"

	SequenceFlow_1tated2 = "SequenceFlow_1tated2"

	SequenceFlow_0tu9bqm = "SequenceFlow_0tu9bqm"

	SequenceFlow_0ibwzxj = "SequenceFlow_0ibwzxj"

	FusionTask = "FusionTask"

	ContouringTask = "ContouringTask"

	MDContouringTask = "MDContouringTask"

	ExclusiveGateway_1clbvj4 = "ExclusiveGateway_1clbvj4"

	ExclusiveGateway_0k2bk5r = "ExclusiveGateway_0k2bk5r"

	ParallelGateway_09iq80z = "ParallelGateway_09iq80z"

	ParallelGateway_06j37h3 = "ParallelGateway_06j37h3"

	CreatePlanScriptTask = "CreatePlanScriptTask"

	PlanningEndEvent = "PlanningEndEvent"
)

func NewStartPlanProcess(id string, bpmnEngine *streamlet.BPMNEngine, startElement streamlet.ElementStateProvider, plan Plan) *streamlet.ProcessInstance {
	impl := StartPlanProcess{sync.RWMutex{}, &plan}
	pi := streamlet.ProcessInstance{Key: "StartPlanProcess", Id: id, BpmnEngine: bpmnEngine, Status: streamlet.ElementCreated, Created: time.Now(), Version: "1", StartElement: startElement, Impl: &impl}
	return &pi
}

type StartPlanProcess struct {
	mu   sync.RWMutex
	plan *Plan
}

func (p *StartPlanProcess) ProcessInstanceData() any {
	return *p.plan
}

func UnmarshalTask(r io.Reader) (streamlet.CompleteUserTasksCmd, error) {
	tasksCmd := streamlet.CompleteUserTasksCmd{Tasks: make([]streamlet.CompleteUserTaskCmd, 0)}
	var v struct {
		Tasks []struct {
			ProcessInstanceId string
			TaskId            string
			TaskKey           string
			Data              json.RawMessage
		}
	}
	if err := json.NewDecoder(r).Decode(&v); err != nil {
		return tasksCmd, err
	}

	for _, t := range v.Tasks {
		taskCmd := streamlet.CompleteUserTaskCmd{
			ProcessInstanceId: t.ProcessInstanceId,
			TaskId:            t.TaskId,
			Data:              nil,
		}
		switch t.TaskKey {

		case FusionTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case ContouringTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		case MDContouringTask:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData

		default:
			var taskData streamlet.BaseTask
			dec := json.NewDecoder(bytes.NewReader(t.Data))
			dec.DisallowUnknownFields()
			if err := dec.Decode(&taskData); err != nil {
				return tasksCmd, err
			}
			taskCmd.Data = taskData
		}
		tasksCmd.Tasks = append(tasksCmd.Tasks, taskCmd)
	}
	return tasksCmd, nil
}

func TaskData[T any](engine *streamlet.BPMNEngine, key string) T {
	states, err := engine.StateStore.GetElementStates(streamlet.UserTaskType, key)
	if states == nil || err != nil {
		var v T
		return v
	}
	return states[len(states)-1].Object.(*streamlet.UserTask[T]).Data
}

func (p *StartPlanProcess) GetNextElement(engine *streamlet.BPMNEngine, g streamlet.IdGenerator, currentElement streamlet.ElementStateProvider) streamlet.ElementStateProvider {
	switch currentElement.GetElementState().Key {

	case StartEvent_1:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_1rgyt8e, id)

	case SequenceFlow_1rgyt8e:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			p.mu.RLock()
			defer p.mu.RUnlock()
			if !p.plan.Fusion {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlow_0tnr42a, id)
			} else {
				id := g.GenerateId(streamlet.SequenceFlowType)
				return streamlet.NewSequenceFlow(SequenceFlow_0hbc8s4, id)
			}
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_1clbvj4, id, gwHandler)

	case SequenceFlow_0hbc8s4:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](FusionTask, id)

	case SequenceFlow_04cepq3:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			id := g.GenerateId(streamlet.SequenceFlowType)
			return streamlet.NewSequenceFlow(SequenceFlow_0zwsz2i, id)
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0k2bk5r, id, gwHandler)

	case SequenceFlow_0tnr42a:
		id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {
			id := g.GenerateId(streamlet.SequenceFlowType)
			return streamlet.NewSequenceFlow(SequenceFlow_0zwsz2i, id)
		}
		return streamlet.NewExclusiveGateway(ExclusiveGateway_0k2bk5r, id, gwHandler)

	case SequenceFlow_0zwsz2i:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0zwsz2i}
		outSeqKeys := []string{SequenceFlow_1fdvgyu, SequenceFlow_11ovoow}
		return streamlet.NewParallelGateway(ParallelGateway_09iq80z, id, SequenceFlow_0zwsz2i, inSeqKeys, outSeqKeys)

	case SequenceFlow_1fdvgyu:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](ContouringTask, id)

	case SequenceFlow_11ovoow:
		id := g.GenerateId(streamlet.UserTaskType)
		return streamlet.NewUserTask[streamlet.BaseTask](MDContouringTask, id)

	case SequenceFlow_0dpgu80:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0dpgu80, SequenceFlow_1tated2}
		outSeqKeys := []string{SequenceFlow_0tu9bqm}
		return streamlet.NewParallelGateway(ParallelGateway_06j37h3, id, SequenceFlow_0dpgu80, inSeqKeys, outSeqKeys)

	case SequenceFlow_1tated2:
		id := g.GenerateId(streamlet.ParallelGatewayType)
		inSeqKeys := []string{SequenceFlow_0dpgu80, SequenceFlow_1tated2}
		outSeqKeys := []string{SequenceFlow_0tu9bqm}
		return streamlet.NewParallelGateway(ParallelGateway_06j37h3, id, SequenceFlow_1tated2, inSeqKeys, outSeqKeys)

	case SequenceFlow_0tu9bqm:
		id := g.GenerateId(streamlet.ScriptTaskType)
		script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {
			fmt.Sprintln("Created Plan")
		}
		return streamlet.NewScriptTask(CreatePlanScriptTask, id, script)

	case SequenceFlow_0ibwzxj:
		id := g.GenerateId(streamlet.NoneEndEventType)
		return streamlet.NewNoneEndEvent(PlanningEndEvent, id)

	case FusionTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_04cepq3, id)

	case ContouringTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0dpgu80, id)

	case MDContouringTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_1tated2, id)

	case ExclusiveGateway_1clbvj4:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ExclusiveGateway_0k2bk5r:
		gw := currentElement.(*streamlet.ExclusiveGateway)
		return gw.NextElement

	case ParallelGateway_09iq80z:
		return nil

	case ParallelGateway_06j37h3:
		return nil

	case CreatePlanScriptTask:
		id := g.GenerateId(streamlet.SequenceFlowType)
		return streamlet.NewSequenceFlow(SequenceFlow_0ibwzxj, id)

	case PlanningEndEvent:
		return nil

	}
	return nil
}

package plan

import (
	"fmt"
	"gitlab.com/gostreamlet/streamlet/streamlet"
	"strconv"
	"strings"
	"testing"
	"time"
)

func autoCompleteTasks(engine *streamlet.BPMNEngine) {
	for {
		if engine.ProcessInstanceCount() == 0 {
			time.Sleep(1 * time.Millisecond)
		} else {
			break
		}
	}
	//fmt.Println("Process instances started.")
	for {
		piCmdResp, err := engine.SendCommand(streamlet.GetProcessInstancesCmd{})
		if err != nil {
			engErr, ok := err.(streamlet.BPMNEngineError)
			if ok {
				if strings.Contains(engErr.Message, "cancelled") {
					return
				}
			}
			fmt.Println(err)
			continue
		}
		piResp := piCmdResp.(streamlet.GetProcessInstancesResp)
		if len(piResp.Errors) > 0 {
			//fmt.Printf("autocomplete errors: %+v\n", len(piResp.Errors))
			continue
		}
		if piResp.Data == nil || len(piResp.Data) == 0 {
			//fmt.Println("getProcessInstanceResp: no data or errors")
			return
		}
		taskCmdResp, err := engine.SendCommand(streamlet.GetTasksCmd{})
		if err != nil {
			fmt.Println(err)
			continue
		}
		taskResp := taskCmdResp.(streamlet.GetTasksResp)
		//nTasks := len(taskResp.Data)
		//fmt.Printf("Tasks: %d\n", nTasks)
		for _, t := range taskResp.Data {
			data := streamlet.BaseTask{
				CompletedBy: streamlet.CompletedBy{
					CompletedById:   "123",
					CompletedByUser: "John Doe",
				},
				Comments: streamlet.Comments{Comments: "Comments"},
			}
			_, err := engine.SendCommand(streamlet.CompleteUserTasksCmd{Tasks: []streamlet.CompleteUserTaskCmd{{
				ProcessInstanceId: t.ProcessInstanceId,
				TaskId:            t.Id,
				Data:              data,
			}}})
			if err != nil {
				fmt.Println(err)
				continue
			}
			/*
				Check for errors
				tasksResp := resp.(streamlet.CompleteTasksResp)
				for _, taskResp := range tasksResp.TasksResp {
					if taskResp.Error != nil {
						log.Println(taskResp.Error)
					}
				}*/
		}
		//}
		time.Sleep(1 * time.Millisecond)
	}
}

func TestPlanFusion(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewStartPlanProcess(id, engine, startEvent, Plan{Fusion: true})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	/*checkState := func(elementType flow.ElementType, gotStates []flow.StateEntry, key, id string, index int, status flow.ElementState) {
		wantState := flow.StateEntry{
			ElementType: elementType,
			Key:         key,
			Id:          id,
			Status:      status,
		}
		if gotStates[index] != wantState {
			t.Fatalf("Got %v, expected %v", gotStates[index], wantState)
		}
	}
	fusionStates, _ := engine.StateHistoryStore().GetStateHistory(flow.UserTaskType, "2")
	checkState(flow.UserTaskType, fusionStates, "Fusion", "2", 3, flow.ElementCompleted)
	contouringStates, _ := engine.StateHistoryStore().GetStateHistory(flow.UserTaskType, "3")
	checkState(flow.UserTaskType, contouringStates, "Contouring", "3", 3, flow.ElementCompleted)*/
}

func TestPlanNoFusion(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewStartPlanProcess(id, engine, startEvent, Plan{Fusion: false})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "Fusion", streamlet.ElementCompleted, 0)
	checkState(streamlet.UserTaskType, "ContouringTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "MDContouringTask", streamlet.ElementCompleted, 1)
}

func TestTwoPlans(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewStartPlanProcess("P1", engine, startEvent, Plan{Fusion: false})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	startEvent = streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p = NewStartPlanProcess("P2", engine, startEvent, Plan{Fusion: true})
	engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType string, key string, status string, count int) {
		histories := engine.StateHistoryStore.GetStateHistories()
		got := 0
		for _, h := range histories {
			if h.Key == key && h.Status == status {
				got++
			}
		}
		if got != count {
			t.Errorf("Got '%d' %v %s %v, expected '%d'", got, elementType, key, status, count)
		}
	}
	checkState(streamlet.UserTaskType, "FusionTask", streamlet.ElementCompleted, 1)
	checkState(streamlet.UserTaskType, "ContouringTask", streamlet.ElementCompleted, 2)
	checkState(streamlet.UserTaskType, "MDContouringTask", streamlet.ElementCompleted, 2)
}

func TestUpdatePlanCmd(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	p := NewStartPlanProcess("P1", engine, startEvent, Plan{Fusion: false})
	resp, err := engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	if err != nil {
		t.Errorf("got unexpected error: %s", err)
	}
	startResp := resp.(streamlet.StartProcessInstanceResp)
	if startResp.Err != nil {
		t.Errorf("got unexpected error: %s", startResp.Err)
	}
	result, err := engine.SendCommand(streamlet.ProcessInstanceCmd{Id: "P1", Cmd: UpdatePlanCmd{Plan: Plan{Fusion: true, Score: 2}}})
	if err != nil {
		t.Errorf("Received error sending UpdatePlanCmd: %s", err)
	}
	if err, ok := result.(streamlet.BPMNEngineError); ok {
		t.Errorf("Received error sending UpdatePlanCmd: %+v", err)
	}

	if result.(*Plan).Fusion != true {
		t.Errorf("Got fusion false, wanted true")
	}
	result, err = engine.SendCommand(streamlet.ProcessInstanceCmd{Id: "P1", Cmd: GetPlanCmd{}})
	if err != nil {
		t.Errorf("Received error sending GetPlanCmd")
	}
	plan := result.(*Plan)
	if !plan.Fusion {
		t.Errorf("Got fusion false, wanted true.")
	}
	got := plan.Score
	want := 2
	if got != want {
		t.Errorf("Got score %d, want %d", got, want)
	}
}

func TestManualBenchmark(t *testing.T) {
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	const count = 1000
	begin := time.Now()
	for n := 0; n < count; n++ {
		startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
		p := NewStartPlanProcess("P"+strconv.Itoa(n), engine, startEvent, Plan{Fusion: false})
		engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
	}
	processesStarted := time.Now()
	fmt.Printf("Processes started at %v: \n", processesStarted)
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	completed := time.Now()
	fmt.Printf("Start %d instances time: %v\n", count, processesStarted.Sub(begin))
	fmt.Printf("Complete %d instances time: %v\n", count, completed.Sub(begin))
	piCount := engine.ProcessInstanceCount()
	if piCount != 0 {
		t.Fatalf("Expected 0 process instances, got %d", piCount)
	}
}

func BenchmarkPlanMemoryStore(b *testing.B) {
	b.Logf("b.N: %d", b.N)
	engine := streamlet.NewMemoryBPMNEngine()
	engine.Start()
	b.ReportAllocs()
	b.ResetTimer()
	created := false
	for n := 0; n < b.N; n++ {
		startEvent := streamlet.NewNoneStartEvent("StartEvent_1", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
		p := NewStartPlanProcess("P"+strconv.Itoa(n), engine, startEvent, Plan{Fusion: false})
		engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: p})
		if created && n > 100 {
			created = false
			go autoCompleteTasks(engine)
		}
	}
	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
}

package main

import (
	"flag"
	"fmt"
	"gitlab.com/gostreamlet/streamlet/builder"
	"os"
	"path/filepath"
)

var input = flag.String("input", ".", "input bpmn file or directory containing bpmn files")
var output = flag.String("output", ".", "output directory for code")

func main() {
	flag.Parse()
	genBPMN()
}

func genBPMN() {
	if !builder.IsBPMN(*input) && !builder.IsDir(*input) {
		fmt.Printf("input '%s' must be a directory or file ending with .bpmn\n", *input)
		return
	}
	dir, err := os.Getwd()
	if err != nil {
		fmt.Printf("error getting current directory: %v\n", err)
	}
	*input = filepath.Join(dir, *input)
	*output = filepath.Join(dir, *output)
	fmt.Printf("Processing input: '%s'\n", *input)
	pg := builder.NewProcessGenerator(*input, *output)
	err = pg.GenerateAndWriteCodeFromBPMN()
	if err != nil {
		fmt.Printf("error: '%v'\n", err)
		return
	}
}

package builder

import (
	"bytes"
	_ "embed"
	"encoding/xml"
	"errors"
	"fmt"
	"gitlab.com/gostreamlet/streamlet/streamlet"
	"go/format"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"time"
)

//go:embed "process.got"
var gotProcess string

type IDName interface {
	GetId() string
	GetName() string
}

type XMLIDName struct {
	Id   string `xml:"id,attr"`
	Name string `xml:"name,attr"`
}

func (x *XMLIDName) GetId() string {
	return x.Id
}

func (x *XMLIDName) GetName() string {
	return x.Name
}

type XMLIncomingElements struct {
	Incoming []string `xml:"incoming"`
}

type Incoming interface {
	GetIncoming() []string
}

type Outgoing interface {
	GetOutgoing() []string
}

type XMLOutgoingElements struct {
	Outgoing []string `xml:"outgoing"`
}

func (e *XMLIncomingElements) GetIncoming() []string {
	return e.Incoming
}

func (e *XMLOutgoingElements) GetOutgoing() []string {
	return e.Outgoing
}

type XMLTimerEventDefinition struct {
	XMLName xml.Name `xml:"timerEventDefinition"`
	XMLTimeCycle
}

type XMLTimeCycle struct {
	XMLName       xml.Name `xml:"timeCycle"`
	Type          string   `xml:"type,attr"`
	TimeCycle     string   `xml:"timeCycle"`
	ISO8601       streamlet.ISO8601DateTime
	DurationMicro int64
}

type XMLConditionExpression struct {
	XMLName             xml.Name `xml:"conditionExpression"`
	Type                string   `xml:"type,attr"`
	ConditionExpression string   `xml:"conditionExpression"`
}

type XMLBPMNDefinitions struct {
	XMLName xml.Name       `xml:"definitions"`
	Process XMLBPMNProcess `xml:"process"`
}

type Script interface {
	GetScript() string
}

type XMLScript struct {
	Script string `xml:"script"`
}

func (s *XMLScript) GetScript() string {
	return s.Script
}

type XMLBPMNProcess struct {
	XMLIDName
	XMLName             xml.Name              `xml:"process"`
	StartEvents         []XMLBPMNStartEvent   `xml:"startEvent"`
	ExclusiveGateways   []XMLExclusiveGateway `xml:"exclusiveGateway"`
	ParallelGateways    []XMLParallelGateway  `xml:"parallelGateway"`
	SequenceFlows       []XMLSequenceFlow     `xml:"sequenceFlow"`
	UserTasks           []XMLUserTask         `xml:"userTask"`
	EndEvents           []XMLEndEvent         `xml:"endEvent"`
	ScriptTasks         []XMLScriptTask       `xml:"scriptTask"`
	GoPackage           string                `xml:"package,attr"`
	GoStartVariableName string                `xml:"startVariableName,attr"`
	GoStartVariableType string                `xml:"startVariableType,attr"`
}

type XMLBPMNStartEvent struct {
	XMLName xml.Name `xml:"startEvent"`
	XMLIDName
	XMLOutgoingElements
	TimerEventDefinition XMLTimerEventDefinition `xml:"timerEventDefinition"`
}

type XMLExclusiveGateway struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName xml.Name `xml:"exclusiveGateway"`
	Default string   `xml:"default,attr"`
}

type XMLParallelGateway struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName xml.Name `xml:"parallelGateway"`
}

type XMLSequenceFlow struct {
	XMLIDName
	XMLConditionExpression
	XMLName   xml.Name `xml:"sequenceFlow"`
	SourceRef string   `xml:"sourceRef,attr"`
	TargetRef string   `xml:"targetRef,attr"`
}

func (x *XMLSequenceFlow) GetOutgoing() []string {
	return []string{x.TargetRef}
}

type XMLUserTask struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLName    xml.Name `xml:"userTask"`
	GoDataType string   `xml:"dataType,attr"`
}

type XMLEndEvent struct {
	XMLIDName
	XMLIncomingElements
	XMLName xml.Name `xml:"endEvent"`
}

type XMLScriptTask struct {
	XMLIDName
	XMLIncomingElements
	XMLOutgoingElements
	XMLScript
	XMLName xml.Name `xml:"scriptTask"`
}

type Process struct {
	Id                  string
	Name                string
	StartEvent          *Element
	Elements            []*Element
	UserTasks           []*Element
	elementMap          map[string]*Element
	GoPackage           string
	GoStartVariableName string
	GoStartVariableType string
}

type Element struct {
	Id            string
	Name          string
	BpmnType      string
	XMLElement    any
	NextStateCode string
}

type ProcessCode struct {
	Process *Process
	Code    *string
}

type OutputDefProcessCode struct {
	InputFile  string // Input BPMN file path including file name
	OutputFile string // Output go code path including file name
	Defs       *XMLBPMNDefinitions
	OutputCode *ProcessCode // Output code
}

func NewProcessGenerator(input, output string) *ProcessGenerator {
	return &ProcessGenerator{Input: input, Output: output, OutputGen: make(map[string]*OutputDefProcessCode)}
}

// ProcessGenerator manages creating go code for a process
type ProcessGenerator struct {
	Input                   string                                                                                         // Input BPMN file or path to process BPMN files
	Output                  string                                                                                         // Output BPMN file or path for generated code
	OutputFileNameGenerator func(inputFile string, process *XMLBPMNProcess, input, output string, isOutputDir bool) string // Generates the output file path/name for a given input file, and the input/output args.
	OutputGen               map[string]*OutputDefProcessCode                                                               // Stores output definitions and code given input file path and name
}

func (p *ProcessGenerator) GenerateAndWriteCodeFromBPMN() error {
	// Get input path info
	inputFileInfo, err := os.Stat(p.Input)
	if err != nil {
		return err
	}

	// Get output path info
	outputFileInfo, err := os.Stat(p.Output)
	if err != nil {
		return err
	}

	// Get bpmn files to process
	inputFileNames := make([]string, 0)
	if inputFileInfo.IsDir() {
		// Input is a directory, ensure output is a directory
		if !outputFileInfo.IsDir() {
			return errors.New("output must be a directory since input is a directory. got an output file instead")
		}

		// Read all BPMN input files in directory
		files, err := os.ReadDir(p.Input)
		if err != nil {
			log.Fatal(err)
		}
		for _, file := range files {
			fileName := file.Name()
			if strings.HasSuffix(fileName, ".bpmn") && !file.IsDir() {
				inFile := path.Join(p.Input, file.Name())
				inputFileNames = append(inputFileNames, inFile)
			}
		}
	} else {
		// Input is a specific file
		inputFileNames = append(inputFileNames, p.Input)
	}

	// Generate go code from bpmn files
	for _, inputFile := range inputFileNames {
		fmt.Printf("Processing file: %s\n", inputFile)

		// Parse definitions
		defs, err := parseBPMNModelFromFile(inputFile)
		if err != nil {
			return err
		}

		// Generate code
		pc, err := generateCode(defs)
		if err != nil {
			return err
		}

		// Determine output file location and name
		outputFile := p.Output
		if p.OutputFileNameGenerator != nil {
			outputFile = p.OutputFileNameGenerator(inputFile, &defs.Process, p.Input, p.Output, outputFileInfo.IsDir())
		} else {
			if outputFileInfo.IsDir() {
				inputFileName := filepath.Base(inputFile)
				outputFileName := inputFileName[:len(inputFileName)-len(filepath.Ext(inputFileName))] + "_gen.go"
				outputFile = path.Join(p.Output, outputFileName)
			}
		}

		// Write output file
		err = os.WriteFile(outputFile, []byte(*pc.Code), 0666)
		if err != nil {
			fmt.Printf("Error writing file: %v\n", err)
			continue
		}

		// Create and store outputGen
		outputGen := OutputDefProcessCode{InputFile: inputFile, OutputFile: outputFile, Defs: defs, OutputCode: pc}
		p.OutputGen[inputFile] = &outputGen

		// Format/update imports
		fmt.Printf("Output file: %s\n", outputFile)
		cmd := exec.Command("goimports", "-w", outputFile)
		err = cmd.Run()
		if err != nil {
			return err
		}
		fmt.Printf("Format/Imports file: %s\n", outputFile)
	}
	return err
}

func parseBPMNModelFromFile(path string) (*XMLBPMNDefinitions, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return parseBPMNModelFromString(string(b))
}

func parseBPMNModelFromString(bpmn string) (*XMLBPMNDefinitions, error) {
	var defs XMLBPMNDefinitions
	err := xml.Unmarshal([]byte(bpmn), &defs)
	return &defs, err
}

func generateProcess(defs *XMLBPMNDefinitions) (*Process, error) {
	setGoDefaults(defs)
	process := Process{Id: defs.Process.Id, Name: defs.Process.Name, GoPackage: defs.Process.GoPackage, GoStartVariableName: defs.Process.GoStartVariableName, GoStartVariableType: defs.Process.GoStartVariableType}
	if err := validateProcessMeta(process); err != nil {
		return &process, err
	}
	process.elementMap = make(map[string]*Element, 0)
	addElements(&process, defs)
	generateProcessNextStateCodes(&process, defs)
	return &process, nil
}

func setGoDefaults(defs *XMLBPMNDefinitions) {
	if defs.Process.GoPackage == "" {
		defs.Process.GoPackage = strings.ToLower(defs.Process.Id)
	}
	if defs.Process.GoStartVariableName == "" {
		defs.Process.GoStartVariableName = "data"
	}
	if defs.Process.GoStartVariableType == "" {
		defs.Process.GoStartVariableType = "Data"
	}
}

func validateProcessMeta(process Process) error {
	if process.GoPackage == "" {
		return errors.New("missing go package name in .bpmn")
	}
	return nil
}

func NewElement(elementType string, src any) *Element {
	idName, ok := src.(IDName)
	if !ok {
		return nil
	}
	element := Element{
		Id:            idName.GetId(),
		Name:          idName.GetName(),
		BpmnType:      elementType,
		XMLElement:    src,
		NextStateCode: "",
	}
	return &element
}

func addElements(process *Process, defs *XMLBPMNDefinitions) {
	for i, xe := range defs.Process.StartEvents {
		xe := xe
		if xe.TimerEventDefinition.TimeCycle != "" {
			d, err := streamlet.NewISO8601DateTime(xe.TimerEventDefinition.TimeCycle)
			if err != nil {
				log.Fatalf("error: %+v", err)
			}
			dur, err := d.Duration.Duration()
			if err != nil {
				log.Fatalf("error: %+v", err)
			}
			durInt := int64(dur / time.Microsecond)
			xe.TimerEventDefinition.ISO8601 = d
			xe.TimerEventDefinition.DurationMicro = durInt
			defs.Process.StartEvents[i].TimerEventDefinition.ISO8601 = d
			defs.Process.StartEvents[i].TimerEventDefinition.DurationMicro = durInt

		}
		element := NewElement("startEvent", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
	if len(defs.Process.StartEvents) > 0 {
		process.StartEvent = process.elementMap[defs.Process.StartEvents[0].Id]
	}
	for _, xe := range defs.Process.SequenceFlows {
		xe := xe
		element := NewElement("sequenceFlow", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
	for _, xe := range defs.Process.UserTasks {
		xe := xe
		element := NewElement("userTask", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
		process.UserTasks = append(process.UserTasks, element)
	}
	for _, xe := range defs.Process.ExclusiveGateways {
		xe := xe
		element := NewElement("exclusiveGateway", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
	for _, xe := range defs.Process.ParallelGateways {
		xe := xe
		element := NewElement("parallelGateway", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
	for _, xe := range defs.Process.ScriptTasks {
		xe := xe
		element := NewElement("scriptTask", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
	for _, xe := range defs.Process.EndEvents {
		xe := xe
		element := NewElement("endEvent", &xe)
		process.Elements = append(process.Elements, element)
		process.elementMap[xe.Id] = element
	}
}

func warnMissingElement(e string) {
	log.Fatalf("Missing element '%v' when setting incoming/outgoing elements", e)
}

func generateElementNextStateCode(element any, process *Process) {
	var el IDName
	el = element.(IDName)
	currentElement, ok := process.elementMap[el.GetId()]
	if !ok {
		warnMissingElement(el.GetId())
	}
	var xmlOutgoing Outgoing
	xmlOutgoing, ok = (element).(Outgoing)
	if !ok {
		currentElement.NextStateCode = "return nil"
		return
	}
	outgoingElements := getOutgoingElements(xmlOutgoing, process)
	currentElement.NextStateCode = getNextStateCode(currentElement, outgoingElements, process)
}

func getOutgoingElements(xmlOutgoing Outgoing, process *Process) []*Element {
	outgoingElements := make([]*Element, 0)
	for _, outgoing := range xmlOutgoing.GetOutgoing() {
		outEl, ok := process.elementMap[outgoing]
		if !ok {
			warnMissingElement(outgoing)
		}
		outgoingElements = append(outgoingElements, outEl)
	}
	return outgoingElements
}

func generateProcessNextStateCodes(process *Process, defs *XMLBPMNDefinitions) {
	for _, e := range defs.Process.StartEvents {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.UserTasks {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.SequenceFlows {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.EndEvents {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.ExclusiveGateways {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.ParallelGateways {
		generateElementNextStateCode(&e, process)
	}
	for _, e := range defs.Process.ScriptTasks {
		generateElementNextStateCode(&e, process)
	}
}

func getNextStateCode(currentElement *Element, nextElements []*Element, process *Process) string {
	if currentElement.BpmnType == "exclusiveGateway" {
		// Next element is determined by NextElement method call
		return "gw := currentElement.(*streamlet.ExclusiveGateway)\n\t\treturn gw.NextElement"
	} else if currentElement.BpmnType == "parallelGateway" {
		return "return nil"
	}
	if len(nextElements) == 1 {
		// Process flows with one outgoing element
		nextElement := nextElements[0]
		switch nextElement.BpmnType {
		case "sequenceFlow":
			return "id := g.GenerateId(streamlet.SequenceFlowType)\n" +
				"\t\treturn streamlet.NewSequenceFlow(" + nextElement.Id + ", id)"
		case "userTask":
			taskType := "streamlet.BaseTask"
			t := nextElement.XMLElement.(*XMLUserTask)
			if t.GoDataType != "" {
				taskType = t.GoDataType
			}

			return "id := g.GenerateId(streamlet.UserTaskType)\n" +
				"\t\treturn streamlet.NewUserTask[" + taskType + "](" + nextElement.Id + ", id)"
		case "scriptTask":
			var xmlScript Script
			xmlScript, ok := nextElement.XMLElement.(Script)
			if !ok {
				log.Fatalf("No script found for script task `%s`", nextElement.Id)
			}
			code :=
				`id := g.GenerateId(streamlet.ScriptTaskType)
			script := func(ctx context.Context, bpmnEngine *streamlet.BPMNEngine, token *streamlet.Token, task *streamlet.ScriptTask) {`
			code += xmlScript.GetScript()
			code += `
			}
			return streamlet.NewScriptTask(` + nextElement.Id + `, id, script)`
			return code
		case "endEvent":
			return "id := g.GenerateId(streamlet.NoneEndEventType)\n" +
				"\t\treturn streamlet.NewNoneEndEvent(" + nextElement.Id + ", id)"
		case "exclusiveGateway":
			code :=
				`id := g.GenerateId(streamlet.ExclusiveGatewayType)
		gwHandler := func() streamlet.ElementStateProvider {`
			gwLogic, err := getExclusiveGatewayLogicCode(nextElement, process)
			if err != nil {
				log.Fatalf("Could not get exclusive gateway logic code: " + err.Error())
			}
			code += gwLogic
			code += `
		}
		return streamlet.NewExclusiveGateway(` + nextElement.Id + `, id, gwHandler)`
			return code
		case "parallelGateway":
			code := "id := g.GenerateId(streamlet.ParallelGatewayType)\n"
			incoming, err := getParallelGatewayIncomingKeys(nextElement)
			if err != nil {
				log.Fatalf(err.Error())
			}
			outgoing, err := getParallelGatewayOutgoingKeys(nextElement)
			if err != nil {
				log.Fatalf(err.Error())
			}
			code += "\t\t\tinSeqKeys := " + incoming + "\n"
			code += "\t\t\toutSeqKeys := " + outgoing + "\n"
			code += "\t\treturn streamlet.NewParallelGateway(" + nextElement.Id + ", id, " + currentElement.Id + ", inSeqKeys, outSeqKeys)"
			return code
		}
	}
	return "panic(\"Unknown Next Event\")"
}

func getParallelGatewayIncomingKeys(element *Element) (string, error) {
	var xmlIncoming Incoming
	xmlIncoming, ok := element.XMLElement.(Incoming)
	if !ok {
		return "", errors.New("no incoming elements on parallel gateway.")
	}
	incoming := make([]string, 0)
	for _, inc := range xmlIncoming.GetIncoming() {
		incoming = append(incoming, fmt.Sprintf("%s", inc))
	}
	return "[]string{" + strings.Join(incoming, ",") + "}", nil
}

func getParallelGatewayOutgoingKeys(element *Element) (string, error) {
	var xmlOutgoing Outgoing
	xmlOutgoing, ok := element.XMLElement.(Outgoing)
	if !ok {
		return "", errors.New("no outgoing elements on parallel gateway.")
	}
	outgoing := make([]string, 0)
	for _, out := range xmlOutgoing.GetOutgoing() {
		outgoing = append(outgoing, fmt.Sprintf("%s", out))
	}
	return "[]string{" + strings.Join(outgoing, ",") + "}", nil
}

func getExclusiveGatewayLogicCode(element *Element, process *Process) (string, error) {
	xmlElementId := element.XMLElement.(IDName).GetId()
	if xmlElementId != element.Id {
		panic(fmt.Sprintf("Got xml element id '%v', want '%v'", xmlElementId, element.Id))
	}
	gw := element.XMLElement.(*XMLExclusiveGateway)
	var defaultFlowElement *Element
	if gw.Default != "" {
		defaultFlowElement = process.elementMap[gw.Default]
	}
	var xmlOutgoing Outgoing
	xmlOutgoing, ok := element.XMLElement.(Outgoing)
	if !ok {
		return "", errors.New("no outgoing elements on exclusive gateway")
	}
	strOutgoing := xmlOutgoing.GetOutgoing()
	if len(strOutgoing) < 1 {
		return "", errors.New("no outgoing elements on exclusive gateway")
	} else if len(strOutgoing) == 1 {
		outElement, ok := process.elementMap[strOutgoing[0]]
		if !ok {
			return "", errors.New("could not look up outgoing element " + strOutgoing[0])
		}
		if outElement.BpmnType != "sequenceFlow" {
			return "", errors.New("expected sequence flow out of exclusive gateway")
		}
		return "\n\t\t\tid := g.GenerateId(streamlet.SequenceFlowType)\n" +
			"\t\t\treturn streamlet.NewSequenceFlow(" + outElement.Id + ", id)", nil
	} else {
		numOutgoing := len(strOutgoing)

		outElements := make([]*Element, numOutgoing, numOutgoing)
		for i, strOut := range strOutgoing {
			outElements[i] = process.elementMap[strOut]
		}
		numElseIfFlow := numOutgoing - 1 // subtract first if condition
		if defaultFlowElement != nil {
			numElseIfFlow -= 1
		}

		// If statement
		firstIndex := 0
		if defaultFlowElement != nil && defaultFlowElement.Id == outElements[0].Id {
			firstIndex = 1
		}
		sf := outElements[firstIndex].XMLElement.(*XMLSequenceFlow)
		outExpression := sf.ConditionExpression
		code := "\t\t\tp.mu.RLock()\n\t\t\tdefer p.mu.RUnlock()"
		code += "\n\t\tif " + outExpression + " {\n\t\t" + getSequenceFlowCode(outElements[firstIndex]) + "\t\t}"

		// Else Ifs
		for i, outEl := range outElements {
			if i == firstIndex || (defaultFlowElement != nil && outEl.Id == defaultFlowElement.Id) {
				continue // Ignore first if and default
			}
			sf := outEl.XMLElement.(*XMLSequenceFlow)
			outExpression := sf.ConditionExpression
			code += " else if " + outExpression + "{\n\t\t" + getSequenceFlowCode(outEl) + "\t\t}"
		}

		// Else
		if defaultFlowElement != nil {
			code += " else {" + getSequenceFlowCode(defaultFlowElement) + "\t\t}"
		} else {
			// No Default Flow
			code += "\npanic(\"No Default Flow!\")\n"
			code += "\nreturn nil"
		}
		return code, nil
	}
}

func getSequenceFlowCode(element *Element) string {
	return "id := g.GenerateId(streamlet.SequenceFlowType)\n" +
		"\t\treturn streamlet.NewSequenceFlow(" + element.Id + ", id)"
}

func generateCode(defs *XMLBPMNDefinitions) (*ProcessCode, error) {
	process, err := generateProcess(defs)
	if err != nil {
		return nil, err
	}

	var temp *template.Template
	temp, err = template.New("Process").
		Funcs(template.FuncMap{"getUserTaskGoDataType": getUserTaskGoDataType}).
		Parse(gotProcess)
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	err = temp.Execute(&buf, process)
	if err != nil {
		fmt.Println("error executing template generation: ")
		return nil, err
	}
	byteCode, err := format.Source([]byte(buf.String()))
	if err != nil {
		fmt.Printf("error formatting code:\n%s\n", buf.String())
		return nil, err
	}
	code := string(byteCode)
	return &ProcessCode{process, &code}, nil
}

func getUserTaskGoDataType(element *Element) string {
	dataType := element.XMLElement.(*XMLUserTask).GoDataType
	if dataType == "" {
		dataType = "streamlet.BaseTask"
	}
	return dataType
}

func IsBPMN(input string) bool {
	return strings.HasSuffix(input, ".bpmn")
}

func IsDir(input string) bool {
	return input == "." || input == "/" || input == "\\"
}

package streamlet

import (
	"errors"
	"sync"
)

var (
	ErrStoreElementNotFound = NewBPMNEngineError(nil, "element not found")
)

type ProcessInstanceWriter interface {
	WriteProcessInstanceState(info ProcessInstanceState) error
}

type ProcessInstanceReader interface {
	ReadProcessInstance(id string) (ProcessInstanceState, error) // Get rid of?
	ReadProcessInstances() ([]ProcessInstanceState, error)
}

type ProcessInstanceStore interface {
	ProcessInstanceReader
	ProcessInstanceWriter
}

type TokenWriter interface {
	WriteToken(info TokenState) error
}

type TokenReader interface {
	ReadTokens() ([]TokenState, error)
}

type TokenStore interface {
	TokenReader
	TokenWriter
}

// StateWriter writes a StateEntry
type StateWriter interface {
	// WriteState writes a StateEntry
	WriteState(state ElementState) error
}

// StateReader reads a StateEntry based on filters
type StateReader interface {
	// GetState returns the current state for a given elementType and id.
	GetState(elementType string, id string) (ElementState, error)
	// GetElementStates returns all states in the store for a specific element type and key
	GetElementStates(elementType string, key string) ([]ElementState, error)
	// GetStates returns all states in the store
	GetStates() ([]ElementState, error)
}

// StateStore combines a state reader and writer
type StateStore interface {
	ProcessInstanceWriter
	ProcessInstanceReader
	TokenWriter
	TokenReader
	StateWriter
	StateReader
}

// NewMemoryStateStore creates a new in memory state store
func NewMemoryStateStore() *MemoryStateStore {
	store := MemoryStateStore{}
	store.statesById = make(map[string]ElementState)
	store.statesByKey = make(map[string][]*ElementState)
	store.processInstances = make(map[string]ProcessInstanceState)
	store.tokens = make(map[string]map[string]TokenState)
	store.tasks = make(map[string]string)
	return &store
}

// MemoryStateStore stores states in memory
type MemoryStateStore struct {
	mu               sync.Mutex
	processInstances map[string]ProcessInstanceState
	tokens           map[string]map[string]TokenState
	statesById       map[string]ElementState    // stores StateEntries based on the element id
	statesByKey      map[string][]*ElementState // stores StateEntries based on the element key
	tasks            map[string]string
}

func (m *MemoryStateStore) WriteProcessInstanceState(info ProcessInstanceState) error {
	m.processInstances[info.Id] = info
	return nil
}

func (m *MemoryStateStore) ReadProcessInstance(id string) (ProcessInstanceState, error) {
	pi, ok := m.processInstances[id]
	if !ok {
		return pi, errors.New("ProcessInstance Not Found")
	}
	return pi, nil
}

func (m *MemoryStateStore) WriteToken(info TokenState) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	tokenMap, ok := m.tokens[info.ProcessInstanceId]
	if !ok {
		tokenMap = make(map[string]TokenState)
		m.tokens[info.ProcessInstanceId] = tokenMap
	}
	switch info.Complete {
	case true:
		delete(tokenMap, info.Id)
		if len(tokenMap) == 0 {
			m.tokens[info.ProcessInstanceId] = nil
			delete(m.tokens, info.ProcessInstanceId)
		}
	case false:
		tokenMap[info.Id] = info
	}
	return nil
}

func (m *MemoryStateStore) ReadTokens() ([]TokenState, error) {
	tokens := make([]TokenState, 0, len(m.tokens))
	for _, tokenMap := range m.tokens {
		for _, token := range tokenMap {
			tokens = append(tokens, token)
		}
	}
	return tokens, nil
}

func (m *MemoryStateStore) ReadProcessInstances() ([]ProcessInstanceState, error) {
	pis := make([]ProcessInstanceState, len(m.processInstances))
	i := 0
	for _, pi := range m.processInstances {
		pis[i] = pi
		i++
	}
	return pis, nil
}

// WriteState writes a StateEntry to the store
func (m *MemoryStateStore) WriteState(state ElementState) error {
	id := getStateID(state.ElementType, state.Id)
	key := getStateKey(state.ElementType, state.Key)
	m.mu.Lock()
	m.statesById[id] = state
	keyStates, ok := m.statesByKey[key]
	if !ok {
		keyStates = make([]*ElementState, 0)
	}
	keyStates = append(keyStates, &state)
	m.statesByKey[key] = keyStates
	m.mu.Unlock()
	// Todo: Need to write memory states in the context of their parent/process
	if state.Object == nil {
		return nil
	}
	/*switch o := state.Object.(type) {
	case ElementState:
		//fmt.Printf("IElement RefElement: %T\n", o.RefElement())
		switch e := o.Object.(type) {
		case *UserTask[DefaultTask]:
			//fmt.Println("Got User Task!")
			result, _ := json.Marshal(e.completed)
			m.mu.Lock()
			m.tasks[id] = string(result)
			m.mu.Unlock()
			//fmt.Printf("Completed By: %+v\n", e.CompletedBy)
		}
	case ProcessInstance:
		//fmt.Println("ProcessInstanceState")
	}*/
	return nil
}

// GetState returns the current state for a given elementType and id.
func (m *MemoryStateStore) GetState(elementType string, id string) (ElementState, error) {
	state, ok := m.statesById[getStateID(elementType, id)]
	if !ok {
		return state, ErrStoreElementNotFound
	}
	return state, nil
}

// GetElementStates returns all states in the store for a specific element type and key
func (m *MemoryStateStore) GetElementStates(elementType string, key string) ([]ElementState, error) {
	values := make([]ElementState, 0, len(m.statesById))
	keyStates, ok := m.statesByKey[getStateKey(elementType, key)]
	if ok {
		for _, s := range keyStates {
			values = append(values, *s)
		}
	}
	return values, nil
}

// GetStates returns all states in the store
func (m *MemoryStateStore) GetStates() ([]ElementState, error) {
	values := make([]ElementState, 0, len(m.statesById))
	for _, value := range m.statesById {
		values = append(values, value)
	}
	return values, nil
}

func getStateID(elementType string, id string) string {
	return elementType + ":" + id
}

func getStateKey(elementType string, key string) string {
	return elementType + ":" + key
}

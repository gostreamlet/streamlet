package streamlet

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ErrProcessStateMachineRequired  = NewBPMNEngineError(nil, "process: process instance implementation must implement ProcessStateMachine")
	ErrProcessInstanceNotRunning    = NewBPMNEngineError(nil, "process: process instance is completed")
	ErrProcessSendCommandTimeout    = NewBPMNEngineError(nil, "process: send command timeout.")
	ErrProcessReceiveCommandTimeout = NewBPMNEngineError(nil, "process: receive command timeout.")
)

type ProcessStateMachine interface {
	GetNextElement(engine *BPMNEngine, g IdGenerator, currentElement ElementStateProvider) ElementStateProvider
}

type ProcessInstanceDataRetriever interface {
	ProcessInstanceData() any
}

type ProcessInstanceState struct {
	Key     string
	Id      string
	Version string
	Status  string
	Created time.Time
	Data    any
}

type TaskState struct {
	ProcessInstanceKey     string
	ProcessInstanceId      string
	ProcessInstanceVersion string
	Key                    string
	Id                     string
	Status                 string
	Data                   any
}

type ProcessInstance struct {
	Mu                      *sync.RWMutex
	Key                     string
	Id                      string
	Version                 string
	Status                  string
	Created                 time.Time
	StartElement            ElementStateProvider
	Tokens                  []*Token
	BpmnEngine              *BPMNEngine
	Impl                    any // Implementation of BPMN Process
	psm                     ProcessStateMachine
	ctx                     context.Context
	twg                     *sync.WaitGroup
	waitingParallelGateways map[string]*WaitingParallelGateway
	cmdReq                  chan any
	cmdResp                 <-chan any
	done                    chan any
	running                 uint32
}

func (p *ProcessInstance) String() string {
	return p.Id + ": " + strconv.Itoa(len(p.Tokens)) + " Tokens"
}

func (p *ProcessInstance) removeToken(token *Token) {
	ti := -1
	for i, t := range p.Tokens {
		if t == token {
			ti = i
			break
		}
	}
	if ti == -1 {
		return
	}
	p.Tokens[ti] = p.Tokens[len(p.Tokens)-1]
	p.Tokens = p.Tokens[:len(p.Tokens)-1]
}

func (p *ProcessInstance) RefElement() any {
	return p
}

func (p *ProcessInstance) Run(ctx context.Context, wg *sync.WaitGroup) error {
	p.ctx = ctx // Needed for sending commands
	p.Mu = &sync.RWMutex{}
	p.done = make(chan any)
	p.waitingParallelGateways = make(map[string]*WaitingParallelGateway)
	psm, ok := p.Impl.(ProcessStateMachine)
	if !ok {
		return ErrProcessStateMachineRequired
	}
	p.psm = psm

	// Create Token
	token := NewToken(p.StartElement, p, p.BpmnEngine)
	p.Tokens = []*Token{token}

	// Note These Two Each Start a Go Routine
	// Should Not Access Mutable Data Without Synchronization
	// Todo: Could we combine these into one go routine?
	p.WriteState()
	p.createCommandProcessor()
	p.runStateMachine(wg)
	p.ping() // Wait for command processor to be ready to receive commands.
	return nil
}

func (p *ProcessInstance) setRunning(running bool) {
	if running {
		atomic.StoreUint32(&p.running, 1)
	} else {
		atomic.StoreUint32(&p.running, 0)
	}
}

func (p *ProcessInstance) Running() bool {
	return atomic.LoadUint32(&p.running) == 1
}

func (p *ProcessInstance) createCommandProcessor() {
	// Create Command Request Channel
	p.cmdReq = make(chan any)

	// Create Command Go Routine
	cmdFunc := func(done <-chan any, cancel <-chan struct{}, reqChan <-chan any) <-chan any {
		respCh := make(chan any)
		go func() {
			defer func() {
				p.setRunning(false)
			}()
			p.setRunning(true)
			for {
				select {
				case <-cancel:
					return
				case <-done:
					return
				case cmd := <-reqChan:
					switch v := cmd.(type) {
					case PingProcessInstanceCmd:
						respCh <- v // Return original cmd as an 'ack'
					case GetProcessInstanceInfoCmd:
						respCh <- p.GetProcessInstanceInfo()
					case GetTasksInfoCmd:
						respCh <- p.GetTasksInfo()
					case CompleteUserTaskCmd:
						respCh <- p.CompleteTask(v)
					case ParallelGateway:
						respCh <- p.ProcessParallelGateway(v)
					default:
						execCmd, ok := p.Impl.(ExecuteCmd)
						if ok {
							respCh <- execCmd.ExecuteCmd(cmd)
						} else {
							respCh <- v // Todo: Implement commands
						}
					}
				}
			}
		}()
		return respCh
	}
	p.cmdResp = cmdFunc(p.done, p.ctx.Done(), p.cmdReq)
}

func (p *ProcessInstance) runStateMachine(wg *sync.WaitGroup) {
	go func() {
		defer wg.Done()
		for {
			switch p.Status {
			case ElementCreated:
				p.Activating()
				p.setStatus(ElementActivating)
			case ElementActivating:
				p.Activated()
				p.setStatus(ElementActivated)
			case ElementActivated:
				p.Completing()
				p.setStatus(ElementCompleting)
			case ElementCompleting:
				p.setStatus(ElementCompleted)
				go p.BpmnEngine.SendCommand(processCompletedCmd{p.Id})
				p.Completed()
			case ElementCompleted:
				close(p.done)
				return
			}
			p.WriteState()
		}
	}()
}

// SendCommand Called from external client to communicate with process instance
func (p *ProcessInstance) SendCommand(cmd any) (any, error) {
	return p.SendCommandWithTimeout(cmd, 5*time.Second)
}

func (p *ProcessInstance) SendCommandWithTimeout(cmd any, timeout time.Duration) (any, error) {
	if !p.Running() {
		return nil, ErrProcessInstanceNotRunning // No channel/go routine listening for commands.
	}
	select {
	case <-p.ctx.Done():
		return nil, errors.New("process: cancelled") // Todo: Return an error
	case p.cmdReq <- cmd:
		return <-p.cmdResp, nil
	case <-time.After(timeout):
		err := ErrProcessSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return nil, err
	}
}

func (p *ProcessInstance) ping() error {
	cmd := PingProcessInstanceCmd{}
	select {
	case <-p.ctx.Done():
		return errors.New("process: cancelled")
	case p.cmdReq <- cmd:
		<-p.cmdResp
		return nil
	case <-time.After(1 * time.Second):
		err := ErrProcessSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return err
	}
	return nil
}

func (p *ProcessInstance) WriteState() {
	state := ProcessInstanceState{
		Key:    p.Key,
		Id:     p.Id,
		Status: p.Status,
	}
	if data, ok := p.Impl.(ProcessInstanceDataRetriever); ok {
		state.Data = data.ProcessInstanceData()
	}
	p.BpmnEngine.WriteProcessInstanceState(state)
}

func (p *ProcessInstance) Activating() {
}

func (p *ProcessInstance) Activated() {
}

func (p *ProcessInstance) Completing() {
	// Token Wait Group
	p.twg = &sync.WaitGroup{}
	p.twg.Add(len(p.Tokens))

	// Run Tokens
	for _, token := range p.Tokens {
		go token.Run(p.ctx, p.twg)
	}
	p.twg.Wait()
}

func (p *ProcessInstance) Completed() {
}

func (p *ProcessInstance) Terminating() {
}

func (p *ProcessInstance) Terminated() {
}

func (p *ProcessInstance) setStatus(status string) {
	p.Mu.Lock()
	defer p.Mu.Unlock()
	p.Status = status
}

func (p *ProcessInstance) GetProcessInstanceInfo() ProcessInstanceState {
	p.Mu.RLock()
	defer p.Mu.RUnlock()
	return ProcessInstanceState{
		Key:     p.Key,
		Id:      p.Id,
		Status:  p.Status,
		Created: p.Created,
	}
}

func (p *ProcessInstance) GetTasksInfo() []TaskState {
	tokens := copyTokenSlice(p)
	taskInfos := make([]TaskState, 0, len(tokens))
	for _, t := range tokens {
		if t.IsComplete() {
			continue
		}
		resp := t.SendCommand(p.ctx, GetUserTaskCmd{}, 1*time.Second)
		if resp == nil || resp == "" {
			continue
		}
		tInfo, ok := resp.(TaskState)
		if !ok {
		}
		taskInfos = append(taskInfos, tInfo)
	}
	return taskInfos
}

func (p *ProcessInstance) CompleteTask(cmd CompleteUserTaskCmd) any {
	resp := NewCompleteTasksResp()
	wg := sync.WaitGroup{}
	tokens := copyTokenSlice(p)
	wg.Add(len(tokens))
	ch := make(chan CompleteUserTaskResp)
	go func() {
		wg.Wait()
		close(ch)
	}()
	// Send complete task command to all process instance tokens, since we don't know which one has the task.
	for _, t := range tokens {
		go func(t *Token, ch chan<- CompleteUserTaskResp) {
			defer wg.Done()
			tResp := t.SendCommand(p.ctx, cmd, 1*time.Millisecond)
			if tResp != nil {
				ch <- tResp.(CompleteUserTaskResp)
			}
			ch <- CompleteUserTaskResp{Error: errors.New("process instance complete task: nil token resp")}
		}(t, ch)
	}
	for tResp := range ch {
		// Responses can come from tokens that don't have the task. Only add responses for the matching task.
		if tResp.ProcessInstanceId == cmd.ProcessInstanceId && tResp.TaskId == cmd.TaskId {
			resp.TasksResp = append(resp.TasksResp, tResp)
		}
	}
	return *resp
}

type WaitingParallelGateway struct {
	key   string
	count int
	done  chan any
}

func (p *ProcessInstance) ProcessParallelGateway(v ParallelGateway) <-chan any {
	wpg, ok := p.waitingParallelGateways[v.Key]
	if !ok {
		done := make(chan any)
		wpg = &WaitingParallelGateway{key: v.Key, count: len(v.InSeqKeys), done: done}
		p.waitingParallelGateways[v.Key] = wpg
	}
	wpg.count--
	if wpg.count == 0 {
		// Lock
		p.Mu.Lock()
		defer p.Mu.Unlock()

		// Create Tokens
		for _, outKey := range v.OutSeqKeys {
			id := (*p.BpmnEngine).GenerateId(SequenceFlowType)
			token := NewToken(NewSequenceFlow(outKey, id), p, p.BpmnEngine)
			p.Tokens = append(p.Tokens, token)
			p.twg.Add(1)
			go token.Run(p.ctx, p.twg)
		}

		// Todo: Delete merged tokens

		// Signal Waiting Tokens to End
		close(wpg.done)
	}
	return wpg.done
}

// copyTokenSlice creates a new slice containing the current process tokens.
func copyTokenSlice(p *ProcessInstance) []*Token {
	p.Mu.RLock()
	defer p.Mu.RUnlock()
	newTokenSlice := make([]*Token, len(p.Tokens))
	for i := range p.Tokens {
		newTokenSlice[i] = p.Tokens[i]
	}
	return newTokenSlice
}

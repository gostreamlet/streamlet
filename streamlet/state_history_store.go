package streamlet

import "sync"

type StateHistoryStore interface {
	WriteStateHistory(state ElementState)
	GetStateHistory(elementType string, id string) ([]ElementState, bool)
	GetStateHistories() []ElementState
}

type MemoryStateHistoryStore struct {
	states map[string][]ElementState
	mu     sync.Mutex
}

func NewMemoryStateHistoryStore() *MemoryStateHistoryStore {
	store := MemoryStateHistoryStore{mu: sync.Mutex{}}
	store.states = make(map[string][]ElementState)
	return &store
}

func (m *MemoryStateHistoryStore) WriteStateHistory(state ElementState) {
	m.mu.Lock()
	defer m.mu.Unlock()
	states, ok := m.states[getStateID(state.ElementType, state.Id)]
	if !ok {
		states = make([]ElementState, 0)
	}
	states = append(states, state)
	m.states[getStateID(state.ElementType, state.Id)] = states
}

func (m *MemoryStateHistoryStore) GetStateHistory(elementType string, id string) ([]ElementState, bool) {
	m.mu.Lock()
	defer m.mu.Unlock()
	states, ok := m.states[getStateID(elementType, id)]
	return states, ok
}

func (m *MemoryStateHistoryStore) GetStateHistories() []ElementState {
	m.mu.Lock()
	defer m.mu.Unlock()
	values := make([]ElementState, 0)
	for _, elementStates := range m.states {
		for _, elementState := range elementStates {
			values = append(values, elementState)
		}
	}
	return values
}

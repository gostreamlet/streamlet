package streamlet

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
	"unicode"
)

type token int

const (
	R       token = iota // Repeat
	P                    // Duration
	T                    // Time
	Y                    // Year
	M                    // Month or Minute
	D                    // Day
	H                    // Hour
	S                    // Second
	INTEGER              // Integer
	DECIMAL              // Decimal
	SLASH                // /
	ILLEGAL              // Illegal
	EOF                  // End of file
)

var (
	eof = rune(0)
)

type ISO8601DateTime struct {
	Value          string
	Duration       Duration
	IsDuration     bool
	IsRepeating    bool
	IsIndefinitely bool
}

func NewISO8601DateTime(value string) (ISO8601DateTime, error) {
	dt, err := NewISO8601Parser(strings.NewReader(value)).Parse()
	dt.Value = value
	return dt, err
}

type Duration struct {
	Value   string
	Year    string
	Month   string
	Week    string
	Day     string
	Hour    string
	Minute  string
	Second  string
	HasTime bool
}

func (d Duration) YearFloat() (float64, error) {
	if d.Year == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Year, 64)
}

func (d Duration) MonthFloat() (float64, error) {
	if d.Month == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Month, 64)
}

func (d Duration) DayFloat() (float64, error) {
	if d.Day == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Day, 64)
}

func (d Duration) HourFloat() (float64, error) {
	if d.Hour == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Hour, 64)
}

func (d Duration) MinuteFloat() (float64, error) {
	if d.Minute == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Minute, 64)
}

func (d Duration) SecondFloat() (float64, error) {
	if d.Second == "" {
		return 0, nil
	}
	return strconv.ParseFloat(d.Second, 64)
}

func (d Duration) Duration() (time.Duration, error) {
	dur := 0 * time.Nanosecond

	// Years
	years, err := d.YearFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(years * 365 * 24 * float64(time.Hour))

	// Months
	months, err := d.MonthFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(months * 365 / 12 * 24 * float64(time.Hour))

	// Days
	days, err := d.DayFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(days * 24 * float64(time.Hour))

	// Hours
	hours, err := d.HourFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(hours * float64(time.Hour))

	// Minutes
	minutes, err := d.MinuteFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(minutes * float64(time.Minute))

	// Seconds
	seconds, err := d.SecondFloat()
	if err != nil {
		return dur, err
	}
	dur += time.Duration(seconds * float64(time.Second))

	return dur, nil
}

func isNumber(ch rune) bool {
	return unicode.IsNumber(ch)
}

func isDuration(ch rune) bool {
	return ch == 'P'
}

func isTime(ch rune) bool {
	return ch == 'T'
}

func isSlash(ch rune) bool {
	return ch == '/'
}

// ISO8601Scanner represents a lexical scanner for ISO8601 durations
type ISO8601Scanner struct {
	r *bufio.Reader
}

// NewISO8601Scanner returns a new instance of an ISO8601Scanner.
func NewISO8601Scanner(r io.Reader) *ISO8601Scanner {
	return &ISO8601Scanner{r: bufio.NewReader(r)}
}

// read reads the next rune from the reader.
func (s *ISO8601Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

// unread places the previously read rune back on the reader.
func (s *ISO8601Scanner) unread() { _ = s.r.UnreadRune() }

// Scan returns the next token and literal value.
func (s *ISO8601Scanner) Scan() (tok token, value string) {
	// Read the next rune.
	ch := s.read()

	if isNumber(ch) {
		s.unread()
		return s.scanNumber()
	}
	str := string(ch)
	switch ch {
	case eof:
		return EOF, ""
	case 'R':
		return R, str
	case 'P':
		return P, str
	case 'T':
		return T, str
	case 'Y':
		return Y, str
	case 'M':
		return M, str
	case 'D':
		return D, str
	case 'H':
		return H, str
	case 'S':
		return S, str
	case '/':
		return SLASH, str
	}

	return ILLEGAL, str
}

// scanNumber consumes the current rune and all contiguous numbers.
func (s *ISO8601Scanner) scanNumber() (tok token, num string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isNumber(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	return INTEGER, buf.String()
}

// ISO8601Parser represents an ISO8601 parser.
type ISO8601Parser struct {
	s   *ISO8601Scanner
	buf struct {
		tok token  // last read token
		val string // last read value
		n   int    // buffer size (max=1)
	}
}

// NewISO8601Parser returns a new instance of ISO8601Parser.
func NewISO8601Parser(r io.Reader) *ISO8601Parser {
	return &ISO8601Parser{s: NewISO8601Scanner(r)}
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
func (p *ISO8601Parser) scan() (tok token, val string) {
	// If we have a token on the buffer, then return it.
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.val
	}

	// Otherwise read the next token from the scanner.
	tok, val = p.s.Scan()

	// Save it to the buffer in case we unscan later.
	p.buf.tok, p.buf.val = tok, val
	return
}

// unscan pushes the previously read token back onto the buffer.
func (p *ISO8601Parser) unscan() { p.buf.n = 1 }

func (p *ISO8601Parser) Parse() (ISO8601DateTime, error) {
	dt := ISO8601DateTime{}
	tok, _ := p.scan()
	if tok == R {
		dt.IsRepeating = true
		if nt, _ := p.scan(); nt == SLASH {
			dt.IsIndefinitely = true
		} else {
			p.unscan()
		}
		tok, _ = p.scan()
	}
	if tok != P {
		return dt, fmt.Errorf("expected duration to start with 'P', got '%v'", tok)
	}
	dt.IsDuration = true

	// Parse values for duration in format: PnYnMnDTnHnMnS
	var inTime bool
	for tok != EOF {
		// Read number
		tok, val := p.scan()

		if tok == EOF {
			break
		} else if tok == T {
			inTime = true
			tok, val = p.scan()
		}

		if tok != INTEGER {
			return dt, fmt.Errorf("unexpected token '%v', expected 'INTEGER'", val)
		}
		n := val

		// Read Date/Time Type
		tok, val = p.scan()

		switch tok {
		case Y:
			dt.Duration.Year = n
		case M:
			if inTime {
				dt.Duration.Minute = n
			} else {
				dt.Duration.Month = n
			}
		case D:
			dt.Duration.Day = n
		case H:
			dt.Duration.Hour = n
		case S:
			dt.Duration.Second = n
		default:
			return dt, fmt.Errorf("unexpected token: '%v'", val)
		}
	}
	return dt, nil
}

package streamlet

import (
	"testing"
	"time"
)

func NewTestProcess(id string, bpmnEngine *BPMNEngine) ProcessInstance {
	pi := ProcessInstance{Id: id, Key: "TestKey", Version: "1", BpmnEngine: bpmnEngine, Status: ElementCreated, Impl: &TestProcess{}}
	return pi
}

type TestProcess struct {
}

func (p *TestProcess) GetNextElement(engine *BPMNEngine, g IdGenerator, currentElement ElementStateProvider) ElementStateProvider {
	if currentElement == nil {
		startId := g.GenerateId(NoneStartEventType)
		currentElement = NewNoneStartEvent("Start", startId)
	}
	key := currentElement.GetElementState().Key
	switch key {
	case "Start":
		id := g.GenerateId(NoneIntermediateThrowEventType)
		return NewNoneIntermediateThrowEvent("ThrowEvent1", id)
	case "ThrowEvent1":
		id := g.GenerateId(UserTaskType)
		return NewUserTask[BaseTask]("Task1", id)
	case "Task1":
		id := g.GenerateId(NoneIntermediateThrowEventType)
		return NewNoneIntermediateThrowEvent("ThrowEvent2", id)
	case "ThrowEvent2":
		id := g.GenerateId(NoneEndEventType)
		return NewNoneEndEvent("EndEvent_1", id)
	case "EndEvent_1":
		return nil
	}
	return nil
}

func autoCompleteTasks(engine *BPMNEngine) {
	// Wait for process instances to start
	for {
		piCmdResp, _ := (*engine).SendCommand(GetProcessInstancesCmd{})
		piResp := piCmdResp.(GetProcessInstancesResp)
		if piResp.Data != nil && len(piResp.Data) > 0 {
			break
		}
		time.Sleep(1 * time.Millisecond)
	}

	// Complete tasks
	for {
		piCmdResp, _ := (*engine).SendCommand(GetProcessInstancesCmd{})
		piResp := piCmdResp.(GetProcessInstancesResp)
		if piResp.Data == nil || len(piResp.Data) == 0 {
			return
		}
		taskCmdResp, _ := (*engine).SendCommand(GetTasksCmd{})
		taskResp := taskCmdResp.(GetTasksResp)
		for _, t := range taskResp.Data {
			(*engine).SendCommand(CompleteUserTasksCmd{Tasks: []CompleteUserTaskCmd{{
				ProcessInstanceId: t.ProcessInstanceId,
				TaskId:            t.Id,
				Data: BaseTask{
					CompletedBy: CompletedBy{
						CompletedById:   "User1",
						CompletedByUser: "Fred",
					}},
			}}})
		}
	}
}

func TestStateEntryEquals(t *testing.T) {
	one := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	two := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	if one != two {
		t.Fatalf("%v != %v, expected ==", one, two)
	}
}

func TestStateEntryNotEquals(t *testing.T) {
	one := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "1",
		Status:      ElementActivating,
	}
	two := ElementState{
		ElementType: ProcessInstanceType,
		Id:          "2",
		Status:      ElementActivating,
	}
	if one == two {
		t.Fatalf("%v == %v, expected !=", one, two)
	}
}

func TestStoreProcessInstanceMemoryStateSingle(t *testing.T) {
	var store ProcessInstanceStore
	store = NewMemoryStateStore()
	state := ProcessInstanceState{
		Id:      "1",
		Key:     "Process",
		Version: "1",
		Status:  ElementActivating,
	}
	store.WriteProcessInstanceState(state)
	states, err := store.ReadProcessInstances()
	if err != nil {
		t.Fatalf("got err %v, expected state.", err)
	}

	if len(states) != 1 {
		t.Fatalf("got %v, expected 1", len(states))
	}

	if states[0] != state {
		t.Fatalf("got %v, expected %v", states[0], state)
	}
}

func TestStoreProcessInstanceMemoryStateReplace(t *testing.T) {
	var store ProcessInstanceStore
	store = NewMemoryStateStore()
	state := ProcessInstanceState{
		Id:      "1",
		Key:     "Process",
		Version: "1",
		Status:  ElementActivating,
	}
	store.WriteProcessInstanceState(state)
	state.Status = ElementActivated
	states, _ := store.ReadProcessInstances()
	got := states[0].Status
	want := ElementActivating
	if got != want {
		t.Fatalf("Got '%v', want '%v'", got, want)
	}
	store.WriteProcessInstanceState(state)
	states, _ = store.ReadProcessInstances()
	got = states[0].Status
	want = ElementActivated
	if got != want {
		t.Fatalf("Got '%v', want '%v'", got, want)
	}
}

func TestProcessInstanceExecuteMemoryStateStore(t *testing.T) {
	engine := NewMemoryBPMNEngine()
	if err := engine.Start(); err != nil {
		t.Fatalf("unexpected error starting engine: %v", err)
	}
	id := "P1"
	p := NewTestProcess(id, engine)
	_, err := engine.SendCommand(StartProcessInstanceCmd{&p})
	if err != nil {
		t.Fatalf("unexpected error starting process: %v", err)
	}

	autoCompleteTasks(engine)
	engine.Wait()
	engine.Stop()
	gotState, err := engine.ProcessInstanceStore.ReadProcessInstance(id)
	wantState := ProcessInstanceState{
		Id:     id,
		Key:    "TestKey",
		Status: ElementCompleted,
	}
	if err != nil || gotState != wantState {
		t.Fatalf("got err %v %+v, expected %+v", err, gotState, wantState)
	}
}

func TestMemoryStateStore_WriteToken(t *testing.T) {
	store := NewMemoryStateStore()
	token := TokenState{
		Id:                "12345",
		ProcessInstanceId: "1",
		CurrentElementId:  "92",
		Complete:          false,
	}
	err := store.WriteToken(token)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err := store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen := len(tokens)
	wantLen := 1
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
	got := tokens[0]
	want := token
	if got != want {
		t.Fatalf("Got token '%+v', want '%+v'", got, want)
	}

	// Write second token
	token2 := TokenState{
		Id:                "12346",
		ProcessInstanceId: "1",
		CurrentElementId:  "98",
		Complete:          false,
	}
	err = store.WriteToken(token2)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 2
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
	got = tokens[1]
	want = token2
	if got != want {
		t.Fatalf("Got token '%+v', want '%+v'", got, want)
	}

	// Complete token 1
	token.Complete = true
	err = store.WriteToken(token)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 1
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
	got = tokens[0]
	want = token2
	if got != want {
		t.Fatalf("Got token '%+v', want '%+v'", got, want)
	}

	// Complete token 2
	token2.Complete = true
	err = store.WriteToken(token2)
	if err != nil {
		t.Fatalf("Got unexpected error writing token: %v", err)
	}
	tokens, err = store.ReadTokens()
	if err != nil {
		t.Fatalf("Got unexpected error reading tokens: %v", err)
	}
	gotLen = len(tokens)
	wantLen = 0
	if gotLen != wantLen {
		t.Fatalf("Got '%d' tokens, expected '%d'", gotLen, wantLen)
	}
}

/*
func TestProcessInstanceMemoryStateHistoryStore(t *testing.T) {
	var engine BPMNEngine
	engine = NewMemoryBPMNEngine()
	engine.Start()
	id := "P1"
	p := NewTestProcess(id, &engine)
	engine.SendCommand(StartProcessInstanceCmd{Instance: &p})
	autoCompleteTasks(&engine)
	engine.Wait()
	engine.Stop()
	checkState := func(elementType ElementType, gotStates []StateEntry, PKey, id string, index int, status ElementState) {
		wantState := StateEntry{
			ElementType: elementType,
			Key:         PKey,
			Id:          id,
			Status:      status,
		}
		if gotStates[index] != wantState {
			t.Fatalf("got %v, expected %v", gotStates[index], wantState)
		}
	}
	piStates, _ := engine.StateHistoryStore().getStateHistory(ProcessInstanceType, id)
	checkState(ProcessInstanceType, piStates, "", id, 0, ElementActivating)
	checkState(ProcessInstanceType, piStates, "", id, 1, ElementActivated)
	checkState(ProcessInstanceType, piStates, "", id, 2, ElementCompleting)
	checkState(ProcessInstanceType, piStates, "", id, 3, ElementCompleted)

	startStates, _ := engine.StateHistoryStore().getStateHistory(NoneStartEventType, "0")
	checkState(NoneStartEventType, startStates, "Start", "0", 0, ElementActivating)
	checkState(NoneStartEventType, startStates, "Start", "0", 1, ElementActivated)
	checkState(NoneStartEventType, startStates, "Start", "0", 2, ElementCompleting)
	checkState(NoneStartEventType, startStates, "Start", "0", 3, ElementCompleted)

	if len(startStates) != 4 {
		t.Fatalf("got %v start states, wanted 4", len(startStates))
	}

	event1States, _ := engine.StateHistoryStore().getStateHistory(NoneIntermediateThrowEventType, "1")
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 0, ElementActivating)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 1, ElementActivated)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 2, ElementCompleting)
	checkState(NoneIntermediateThrowEventType, event1States, "ThrowEvent1", "1", 3, ElementCompleted)

	if len(event1States) != 4 {
		t.Fatalf("got %v intermediate throw event states, wanted 4", len(startStates))
	}

	endStates, _ := engine.StateHistoryStore().getStateHistory(NoneEndEventType, "4")
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 0, ElementActivating)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 1, ElementActivated)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 2, ElementCompleting)
	checkState(NoneEndEventType, endStates, "EndEvent_1", "4", 3, ElementCompleted)

	if len(endStates) != 4 {
		t.Fatalf("got %v end event states, wanted 4", len(endStates))
	}
}
*/

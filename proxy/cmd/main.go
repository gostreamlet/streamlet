package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

// NewProxy takes target host and creates a reverse proxy
func NewProxy(targetHost string) (*httputil.ReverseProxy, error) {
	url, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}

	return httputil.NewSingleHostReverseProxy(url), nil
}

// ProxyRequestHandler handles the http request using proxy
func ProxyRequestHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.RequestURI)
		proxy.ServeHTTP(w, r)
	}
}

func NewFlowProxy(processName, url string) {
	proxy, err := NewProxy(url)
	if err != nil {
		panic(err)
	}
	http.HandleFunc(fmt.Sprintf("/%s/", processName), ProxyRequestHandler(proxy))
}

func main() {
	NewFlowProxy("plan", "http://localhost:8081")
	NewFlowProxy("risk", "http://localhost:8082")
	fmt.Println("Starting reverse proxy.")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

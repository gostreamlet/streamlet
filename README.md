# Streamlet

Streamlet is a lightweight package written in Go for building business processes based on Business Process Model and
Notation 2.0 (BPMN).
The core of streamlet is agnostic to how processes are started, created, or interacted with, making it suitable for
use in command line tools, microservices, web applications, or embedding with software written in another language.
Processes
are compiled to standalone executables enabling deployment in a wide variety of environments.

Process engines are typically designed to store process state in a specific technology such as an SQL database.
This is an appealing approach initially, but it can have deployment, maintenance and performance limitations in the
long-run.

Streamlet core does not manage storing state externally, enabling customization by developers to suit their
requirements.
This enables tradeoffs in performance. For example in some cases, persisting process state is not required, but high
throughput and low latency is desired. In other cases, the ability to recover from a system crash and resume the process
is more important
than lower processing speeds due to serialization. Streamlet lets developers and businesses make these tradeoffs on a
process level, where different processes can have different storage requirements, instead of the entire engine forcing
every process to use the same storage mechanism and schema. This fits well with the database per microservice approach.

## Getting started

The following example creates a command line program that executes a Hello World process.

![helloworld.bpmn](/example/helloworld.svg)

The process takes in an input name, and uses a BPMN script task to print to the console 'Hello [Name]'.
The default Name is set to 'World'.

1. Initialize a new go module

```shell
mkdir helloworld
cd helloworld
go mod init helloworld
```

2. Add streamlet to go dependencies

```shell
go get gitlab.com/gostreamlet/streamlet
```

3. Install the streamlet command line builder

```shell
go install gitlab.com/gostreamlet/streamlet/builder/cmd/strm
```

4. Copy the example .bpmn process from `example\helloworld.bpmn` to your project
5. Generate the go code for the process, which creates `helloworld_gen.go`:

```shell
strm
```

6. Create `helloworld.go` with an Input type for the process to start with:

```go
package helloworld

type Input struct {
	Name string
}
```

7. Create a `cmd` folder and `main.go` to test execution of the process:

```shell
mkdir cmd
cd cmd
```

main.go

```go
package main

import (
	"gitlab.com/gostreamlet/streamlet/streamlet"
	"helloworld"
	"log"
)

func main() {
	// Create the engine
	engine := streamlet.NewMemoryBPMNEngine()

	// Start the engine
	err := engine.Start()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		// Wait for running process to complete
		engine.Wait()

		// Shut down the engine
		engine.Stop()
	}()

	// Create a new process instance with an id, start event, and input
	id := engine.GenerateId(streamlet.ProcessInstanceType)
	start := streamlet.NewNoneStartEvent("StartHelloWorldEvent", engine.IdGenerator.GenerateId(streamlet.NoneStartEventType))
	input := helloworld.Input{Name: "World"}
	processInstance := helloworld.NewHelloWorldProcess(id, engine, start, input)

	// Start the process instance
	_, err = engine.SendCommand(streamlet.StartProcessInstanceCmd{Instance: processInstance})
	if err != nil {
		log.Fatal(err)
	}
}

```

7. Run the program:

```shell
go run .
```

8. Output:

```shell
Hello World
```

## Supported BPMN Elements

The following BPMN elements are currently implemented:

- None Start Event
- Timer Start Event (only repeating time cycles, e.g. R/)
- Exclusive Gateway
- Parallel Gateway
- Script Task (Go)
- User Task
- None End Event

## License

Licensing information to come.
